import React from "react";
import "./style/style.css";

const Loading = () => {
  return (
    <div className="outer">
      <span className="Text">Loading...</span>
      <div className="middle">
        <div className="inner"></div>
      </div>
    </div>
  );
};

export default Loading;
