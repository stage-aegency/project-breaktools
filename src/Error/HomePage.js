import React from "react";
import "./style/styleHome.css"

const HomePage = () => {
  return (
    <div>
      <div className="Home-container">
        <div className="Home-txt-img"></div>
        <div className="Home-img"></div>
        <div className="Home-Message">
          <h2 className="Home-h2">Bienvenue a BreakTool</h2>
          <p className="Home-p">
            Veuillez utiliser la plate-forme Notoriety pour accéder à ce service
          </p>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
