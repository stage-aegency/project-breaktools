<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class APIs extends Controller
{
    public function getAllByUserID(Request $req){
        try {
            $user = DB::table('users')
            ->where('id', $req->id)
            ->first();

            $group = DB::table('groups')
            ->where('id',$user->groupID)
            ->first();
            
            $pauses = DB::select('select
                p.*,
                user.groupID,
                user.pseudo,
                user.notif,
                user.notifDate
            from
                pauses p 
            inner join 
                users user
            on
                p.userID=user.id
            where
                user.groupID = ? 
            order by 
                p.ranking',[$group->id]);

            return [
                "etat" => 1,
                "user" => $user,
                "group" => $group,
                "pauses" => $pauses,
                "time" => (time())
            ];
        } catch (\Throwable $th) {
            return [
                "etat" => 0,
                "
                error" => $th->getMessage()
            ];
        }
    }

    public function demandePause(Request $req){
        try {
            $currentPause = DB::table("pauses")
            ->where([
                ["userID" ,  $req->userID]
            ])->get();

            $lastRank = DB::select("select
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? 
            order by ranking desc
            LIMIT 1",[$req->groupID]);

            $curRank = count($lastRank) == 0 ? 1 : ((int)($lastRank[0]->ranking) +1) ;

            $date =time();
            $group = DB::table('groups')
            ->where("id",$req->groupID)
            ->get();
            

            if(count($currentPause) == 0 ){
                $newPause= DB::table("pauses")
                ->insert(
                    array(
                        "userID" => $req->userID,
                        "dateDemande" => $date,
                        "etat" => 0,
                        "ranking" => $curRank
                    )
                );
                $attent = DB::select("select
                userID,
                ranking,
                groupID
                from pauses inner join users on userID = users.id
                where users.groupID = ? and etat = 0  and users.notif = 1 
                order by ranking asc
                ",[$req->groupID]);

                $pauses = DB::select("select
                userID,
                ranking,
                groupID
                from pauses inner join users on userID = users.id
                where groupID = ? and etat = 1
                order by ranking asc
                ",[$req->groupID]);

                if ($group[0]->isOpen == 1 && (count($attent)+count($pauses)) < $group[0]->max) {
                    $emp =DB::table('users')
                    ->where("id",$req->userID)
                    ->update([
                        "notif" => 1,
                        "notifDate" => time()
                    ]);
                }
                
            }
            return ["etat" => 1,"raking"=>$curRank];
            
        } catch (\Throwable $th) {
            return [
                "etat" => 0,
                "message" => $th
            ];
        }
    }

    public function startPause(Request $req){
        try {
            $emp = DB::table('users')
            ->where([
                ['id', $req->userID]
            ])->update([
                "notif" => 0 
            ]);

            $emps = DB::table('users')
            ->where([
                ['groupID', $req->groupID]
            ])->get();
            $list =[];
           foreach($emps as $e){
            array_push($list,$e->id);
           }

            $pause = DB::table('pauses')
            ->whereIn('userID',$list)->update([
                "passing" => 0
            ]);

            $pause = DB::table('pauses')
            ->where([
                ['userID', $req->userID]
            ])->update([
                "etat" => 1,
                "datePause"=> time(),
                "passing" => 0,
                "awaiting" => 0
            ]);

            return [
                "etat"=>1
            ];
            

        } catch (\Throwable $th) {
            return [
                "etat"=> 0,
                "message" => $th->getMessage()
            ];
        }
    }

    public function finichPause(Request $req){
        try {
            DB::table('pauses')
            ->where("userID","$req->userID")
            ->update([
                "etat"=>2,
                "dateFin"=> time()
            ]);
            $group = DB::table('groups')
            ->where("id",$req->groupID)
            ->first();
            
            $pausesNotif = DB::select("select
            userID,
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? and etat = 0 and users.notif = 1 
            order by ranking asc
            ",[$req->groupID]);

            $pauses1 = DB::select("select
            userID,
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? and etat =1
            order by ranking asc
            ",[$req->groupID]);

            if ($group->max < (count($pauses1)+count($pausesNotif))) {
                return ;
            }

            $pauses = DB::select("select
            userID,
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? and etat = 0 and users.notif = 0  and passing = 0
            order by ranking asc
            limit ?
            ",[$req->groupID ,
            ($group->max - (count($pausesNotif)) -(count($pauses1))) >= 0 ?($group->max - (count($pausesNotif))-(count($pauses1))) : 0]);

            
            $list =[];
            foreach($pauses as $e){
                array_push($list,$e->userID);
            }


            $user=DB::table('users')
                ->whereIn("id",$list)
                ->update([
                    "notif"=>1,
                    "notifDate"=> time()
                ]);

                
            $pausesNotif = DB::select("select
            userID,
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? and etat = 0 and users.notif = 1 
            order by ranking asc
            ",[$req->groupID]);
            
            if ($group->max > (count($pauses1)+count($pausesNotif))) {
                $pauses = DB::select("select
                userID,
                ranking,
                groupID
                from pauses inner join users on userID = users.id
                where groupID = ? and etat = 0 and users.notif = 0 
                order by ranking asc
                limit ?
                ",[$req->groupID ,($group->max - (count($pausesNotif))-(count($pauses1)))]);

                $list =[];
                foreach($pauses as $e){
                    array_push($list,$e->userID);
                }


                $user=DB::table('users')
                ->whereIn("id",$list)
                ->update([
                    "notif"=>1,
                    "notifDate"=> time()
                ]);
            }

            return [
                "pause etat 1"=>count($pauses1),
                "notif"=>count($pausesNotif),
                "pauses"=> $pauses
            ];


        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function resetPauses(Request $req){
        try {
           $emps = DB::table('users')
           ->select('id')
           ->where('groupID',$req->groupID)
           ->get();
            $list =[];
           foreach($emps as $e){
            array_push($list,$e->id);
           }

           DB::table('pauses')
           ->whereIn('userID',$list)
           ->delete();

           DB::table('users')
           ->whereIn("id",$list)
           ->update(["notif"=>0]);
           return 1;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function  passPause (Request $req)
    {
        try {
        $group = DB::table('groups')
        ->where(
            "id",$req->groupID
        )->first();
        $enPause = DB::select("select
        userID,
        ranking,
        groupID
        from pauses inner join users on userID = users.id
        where groupID = ? and etat = 1 
        order by ranking asc
        ",[$req->groupID ]);
        $hasNotif = DB::select("select
        userID,
        ranking,
        groupID
        from pauses inner join users on userID = users.id
        where groupID = ? and etat = 0 and users.notif = 1
        order by ranking asc
        ",[$req->groupID]);
        
        $curPause = DB::table('pauses')
        ->where("userID",$req->userID)
        ->first();

        $pauses = DB::select("select
        userID,
        ranking,
        groupID
        from pauses inner join users on userID = users.id
        where groupID = ? and etat = 0 and users.notif = 0 and ranking > ?
        order by ranking asc
        ",[$req->groupID , $curPause->ranking]);
        $test = 0;

        if (count($pauses)==0) {
            $pauses = DB::select("select
            userID,
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? and etat = 0 and users.notif = 0 
            order by ranking asc
            ",[$req->groupID ]);
        }

        if (is_null($pauses) || count($pauses)==0) {
            return 1;
        }

        $pauseToPass = DB::table('pauses')
        ->where('userID' , $req->userID)
        ->update([
            "passing"=>1,
        ]);

        $user=DB::table('users')
        ->where("id",$req->userID)
        ->update([
            "notif"=>0,
            "notifDate"=> time()
        ]);

        DB::table('pauses')
        ->where("userID",$req->userID)
        ->update([
            "awaiting"=>0
        ]);

        if (($group->max - count($enPause) - count($hasNotif)) <0) {
            return 1 ;
        }

        $user=DB::table('users')
            ->where("id",$pauses[0]->userID)
            ->update([
                "notif"=>1,
                "notifDate"=> time()
            ]);

        return 1;

        } catch (\Throwable $th) {
            return $th;
        }
    }
    

    public function awaitPause(Request $req){
        try {
            $test = DB::table('pauses')
            ->where([
                ["userID",$req->userID],
                ["awaiting",1]
            ])->get();
            if (count($test) != 0) {
                return ["etat"=>1];
            }

            DB::table('pauses')
            ->where("userID",$req->userID)
            ->update([
                "awaiting"=>1
            ]);

            DB::table('users')
            ->where("id",$req->userID)
            ->update([
                "notifDate"=>time()
            ]);
            return[
                "etat"=>1
            ];
        } catch (\Throwable $th) {
            return[
                "etat"=>2,
                "message"=>$th
            ];
        }
    }

    public function updateMax(Request $req){
        try {
            $group = DB::table('groups')
            ->where("id",$req->groupID)
            ->update([
                "max"=> $req->max
            ]);

            $gg = DB::table('groups')
            ->where('id',$req->groupID)
            ->first();
            if ($gg->isOpen == 0) {
                return [
                    "etat"=>1
                ];
            }

            $pausesNotif = DB::select("select
            userID,
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? and etat = 0 and users.notif = 1 
            order by ranking asc
            ",[$req->groupID]);

            $pauses1 = DB::select("select
            userID,
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? and etat =1
            order by ranking asc
            ",[$req->groupID]);
            

            $pauses = DB::select("select
            userID,
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? and etat = 0 and users.notif = 0  and passing = 0
            order by ranking asc
            limit ?
            ",[$req->groupID ,
            ($req->max - (count($pausesNotif)) -(count($pauses1))) >= 0 ?($req->max - (count($pausesNotif))-(count($pauses1))) : 0]);

            if (($req->max - (count($pausesNotif))-(count($pauses1)))<=0) {
                return 1;
            }
            
            $list =[];
            foreach($pauses as $e){
                array_push($list,$e->userID);
            }


            $user=DB::table('users')
                ->whereIn("id",$list)
                ->update([
                    "notif"=>1,
                    "notifDate"=> time()
                ]);

                
            $pausesNotif = DB::select("select
            userID,
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? and etat = 0 and users.notif = 1 
            order by ranking asc
            ",[$req->groupID]);
            
            if ($req->max > (count($pauses1)+count($pausesNotif))) {
                $pauses = DB::select("select
                userID,
                ranking,
                groupID
                from pauses inner join users on userID = users.id
                where groupID = ? and etat = 0 and users.notif = 0 
                order by ranking asc
                limit ?
                ",[$req->groupID ,($req->max - (count($pausesNotif))-(count($pauses1)))]);

                $list =[];
                foreach($pauses as $e){
                    array_push($list,$e->userID);
                }


                $user=DB::table('users')
                ->whereIn("id",$list)
                ->update([
                    "notif"=>1,
                    "notifDate"=> time()
                ]);
            }

            return 1;
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    public function updateEtat(Request $req){
        try {
            $query =DB::table('groups')
            ->where(
                "id",$req->groupID
            )->update([
                "isOpen" => $req->isOpen
            ]);

            $group = DB::table('groups')
            ->where("id" , $req->groupID)
            ->get();

            $pauses = DB::select("select
            userID,
            ranking,
            groupID
            from pauses inner join users on userID = users.id
            where groupID = ? and etat = 1
            order by ranking asc
            ",[$req->groupID]);

            if(count($pauses)< $group[0]->max ){
                $attent = DB::select("select
                userID
                from pauses inner join users on userID = users.id
                where groupID = ? and etat = 0 and passing = 0 and users.notif = 0 and awaiting = 0
                order by ranking asc
                limit ? 
                ",[$req->groupID , ( $group[0]->max - count($pauses))]);

                $list =[];
                foreach($attent as $e){
                    array_push($list,$e->userID);
                }
                $empTeUpdate = DB::table('users')
                ->whereIn("id" , $list)
                ->update([
                    "notif" => 1,
                    "notifDate"=> time()
                ]);

            }
            return[
                "etat"=>1,
                "groupID"=>$req->groupID,
                "isOpen"=>$req->isOpen
            
            ];
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function getAdminData(Request $req){
        try {
            $groups = DB::table('groups')
            ->get();

            
            $list =[];
            foreach($groups as $e){
                $pauses = DB::select('select
                p.*,
                user.groupID,
                user.pseudo,
                user.notif,
                user.notifDate
            from
                pauses p
            inner join 
                users user
            on
                p.userID=user.id
            where
                user.groupID = ? 
            order by 
                p.ranking',[$e->id]);
                array_push($list,[
                    "group"=>$e,
                    "dataGroups"=>[
                        "group"=> $e,
                        "pauses"=>$pauses,
                        "time"=>time()
                    ]]);
            }

            return [
                "etat"=>1,
                "data"=> $list
            ];

        } catch (\Throwable $th) {
            return [
                "etat"=>0,
                "message"=> $th
            ];
        }
    }

    public function getUsersGroups(Request $req){
        try {
            $users = DB::table('users')
            ->where("typeID",1)
            ->get();
            $groups = DB::table('groups')
            ->get();
            return [
                "etat"=>1,
                "users"=>$users,
                "groups"=>$groups
            ];
        } catch (\Throwable $th) {
            return [
                "etat"=>0,
                "message"=>$th
            ];
        }
    }

    public function checkUser(Request $req){
        try {
            $user = DB::table('users')
            ->where("Pseudo",$req->pseudo)
            ->get();
            if (count($user)==0) {
                return[
                    "etata"=>1,
                    "user"=>0
                ];
            } else {
                return[
                    "etata"=>1,
                    "user"=>$user[0]->id
                ];
            }
            
        } catch (\Throwable $th) {
            return[
                "etata"=>0,
                "msg"=>$th
            ];
        }
    }
    public function updateusersGroups(Request $req){
        try {
            $group = DB::table('groups')
            ->where("id" , $req->groupID)
            ->first();

            DB::table('users')
            ->whereIn("Pseudo",$req->pseudos)
            ->update([
                "groupID"=>$req->groupID
            ]);

            $users =DB::table('users')
            ->select("Pseudo")
            ->whereIn("Pseudo",$req->pseudos)
            ->get();

            $listFound = [];
            foreach($users as $e){
                    array_push($listFound,$e->Pseudo);
            }
            $listNotFound = [];
            foreach($req->pseudos as $e){
                if (!in_array($e, $listFound)) {
                    array_push($listNotFound,$e);
                }
            }

            if (count($listNotFound)==0) {
                return [
                    "etat"=>1
                ];
            }

            $newUsers = [];
            foreach($listNotFound as $e){
                array_push($newUsers,[
                    "Pseudo"=>$e,
                    "typeID"=>1,
                    "notif"=>0,
                    "notifDate"=>0,
                    "groupID"=>$req->groupID
                ]);
            }

            DB::table('users')
            ->insert($newUsers);

            return [
                "etat"=>1,
                "deff"=>$listNotFound,
                "users"=>$listFound,
                "req"=>$req->pseudos,
                "groupID"=>$req->groupID
            ];

        } catch (\Throwable $th) {
            return $th;
        }
    }

}
