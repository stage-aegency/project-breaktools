import Loading from "../Loading/Loading";
import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { getUser, getUsers} from "../tools/axios";
import { AdminCard } from "./Admin/AdminCard/AdminCard";
import AttenteListe from "./Agent/AttenteListe/AttenteListe";
import PauseListe from "./Agent/PauseListe/PauseListe";
import "./style/styleAdmins.css";
import RetardListe from "./Admin/RetardListe/RetardListe";
import { RefreshAdmin } from "../tools/vairables";

const Admins = (A) => {
  const [data, setdata] = useState(0);
  const [Users, setUsers] = useState(0);

  const refrech = (id) => {
    getUser(id)
      .then((e) => {
        try {
          setdata(0);
          setdata(e === undefined ? 0 : e.data === undefined ? 0 : e.data);
        } catch (error) {
          setdata(0);
        }
      })
      .catch((e) => {
        setdata(0);
      });
    getUsers().then((e) => {
      setUsers(e);
    });
    return 1;
  };

  useEffect(() => {
    refrech(A.match.params.ID);
    var timer = setInterval(() => {
      refrech(A.match.params.ID);
    }, RefreshAdmin);

    return () => {
      clearInterval(timer);
    };
  }, [A.match.params.ID]);

  try {
    return (
      <div className="adminGrpContainer">
        {data !== 0 && Users !== 0 ? (
          <>
            {data.user.typeID === 2 ? (
              <div>
                <>
                  {Users.data.data.map((e) => (
                    <div className="groupContainer" key={e.group.label}>
                      <h1>{e.group.label}</h1>
                      <AdminCard users={e} data={data} refrech={refrech} />
                      <div className="tablesContainer">
                        <AttenteListe user={e.dataGroups} />
                        <PauseListe user={e.dataGroups} />
                        <RetardListe user={e.dataGroups} />
                      </div>
                    </div>
                  ))}
                </>
              </div>
            ) : (
              <Loading />
            )}
          </>
        ) : (
          ""
        )}
      </div>
    );
  } catch (error) {
    return (
      <div >
        <Loading />
      </div>
    );
  }
};

export default withRouter(Admins);
