import React, { useEffect, useState } from "react";
import MaterialTable from "material-table";
import './style/style.css'
import { ChronoRefresh } from "../../../tools/vairables";


const PauseListe = ({ user, refrech }) => {
  const [data, setdata] = useState([]);

  const secToMin = (Diff) => {
    var Minutes = Math.floor(Diff / 60);
    var Seconds = Diff % 60;
    return Minutes + ":" + (Seconds < 10 ? "0" : "") + Seconds;
  };

  const addSec = (list) => {
    var temp = [...list];
    return temp.map((e) => {
      return {
        ...e,
        diff: e.diff + 1,
        Chrono: secToMin(e.diff + 1),
      };
    });
  };

  var fixdata = (data) => {
    return data.pauses
      .filter((e) => e.etat === 1)
      .map((e) => {
        var dif = user.time - e.datePause;
        return { ...e, Chrono: secToMin(dif), diff: dif };
      });
  };

useEffect(() => {
  if (user !== 0) {
    var pauses = fixdata(user);
    setdata(pauses);
    var timer = setInterval(() => {
      pauses = addSec(pauses);
      setdata(pauses);
    }, ChronoRefresh);
    return () => {
      clearInterval(timer);
    };
  }
}, []);
//
// state Data doit contenir user puis travailler avec 
//
  return (
    <div className="tablePause">
      <MaterialTable
        title="Pauses en cours"
        data={data}
        columns={[
          {
            title: "Ranking",
            field: "ranking",
            cellStyle: {
              backgroundColor: "#e6e6ff",
              textAlign: "center",
            },
          },
          {
            title: "Pseudo",
            field: "pseudo",
            cellStyle: {
              textAlign: "center",
            },
          },
          {
            title: "Chrono",
            field: "Chrono", // a definir
            cellStyle: {
              textAlign: "center",
            },
          },
        ]}
        localization={{
          body: {
            emptyDataSourceMessage: "Aucun agent en pause.",
          },
        }}
        style={{
          width: "100%",
          tableLayout: "fixed",
          boxShadow: "0px 0px 15px 6px rgba(0,0,0,0.1)",
          borderRadius: "20px",
        }}
        options={{
          draggable: false,
          headerStyle: {
            backgroundColor: "#154c79",
            color: "#ffffff",
            textAlign: "center",
            fontWeight: "bold",
            width: "22px",
            position: "sticky",
          },
          maxBodyHeight: "300px",
          minBodyHeight: "300px",
          rowStyle: (rowData) => {
            if (rowData.etat === 1) {
              if (rowData.diff >= 900) {
                return { backgroundColor: "#ffa3a3" };
              }
              return { backgroundColor: "#C1E7C3" };
            }
            return { backgroundColor: "#DAE9ED" };
          },

          search: false,
          paging: false,
        }}
      />
    </div>
  );
};

export default PauseListe;
